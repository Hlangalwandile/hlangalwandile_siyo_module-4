import 'package:flutter/material.dart';
import 'splash_screen.dart';

void main() => runApp(new MainApp());

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SplasScreen',
      theme: ThemeData(
          // Define the default brightness and colors.
          brightness: Brightness.light,
          primaryColor: Color.fromARGB(255, 41, 40, 34),
          backgroundColor: Color.fromARGB(255, 255, 224, 23),

          // Define the default font family.
          fontFamily: 'Poppins'),
      home: SplashScreen(),
    );
  }
}
