import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Home();
  }
}

class _Home extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Welcome")),
      body: Container(
          alignment: Alignment.center,
          height: MediaQuery.of(context).size.height,
          //making container height equal to verticle hight.
          width: MediaQuery.of(context).size.width,
          //making container width equal to verticle width.
          child: Text(
            "MTN App Academy",
            style: TextStyle(
              fontSize: 30,
            ),
          )),
      floatingActionButton: Theme(
        data: Theme.of(context).copyWith(splashColor: Colors.yellow),
        child: FloatingActionButton(
          onPressed: () {},
          child: const Icon(Icons.add),
        ),
      ),
    );
  }
}
